function EnableDisableTextBox(btnPassport) {
    var mandatoryQuestions = document.getElementById("mandatoryQuestions");
    if (btnPassport.value == "Yes") {
        mandatoryQuestions.removeAttribute("disabled");
        document.getElementById("quesYes").checked = true;
        document.getElementById("quesNo").checked = false;
    } else {
        mandatoryQuestions.setAttribute("disabled", "disabled");
        document.getElementById("quesYes").checked = false;
        document.getElementById("quesNo").checked = true;
    }
}