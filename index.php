<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questionnaire - Media Group</title>
    <!-- style -->
    <link rel="stylesheet" href="css/questionnaire.css">
    <!-- js -->
    <script src="js/questionnaire.js"> </script>
</head>
<body>
    <div class="welcome-content">
        <p> Dear Hinduja Media Parivar, </p>
        <p> We hope that you and your families are staying safe and following the simple steps we’ve shared with
         you to ensure your safety and wellbeing. As a precautionary measure, we are requesting all our colleagues
          to provide us with some basic information, solely intended to enable us to be better prepared and render 
          expedited assistance in case of any emergency. All these details shall be kept strictly confidential. </p>
    <form action="api/api.php" method="POST">
    <label for="fname"> FULL NAME : </label>
        <input type="text" id="fname" size="50" name="full_name" required> <br> <br>
        <label for="caddress"> CURRENT RES. ADDRESS : </label>
        <input type="text" id="caddress" size="50" name="current_address" required> <br><br>
        <label for="contact"> CONTACT NUMBER : </label>
        <input type="text" id="contact" size="50" name="contact_number" required> <br><br>
        <label for="bld-grp"> BLOOD GROUP : </label>
        <input type="text" id="bld-grp" size="50" name="blood_group"required > <br><br>
        <label for="dob"> D.O.B : </label>
        <input type="date" id="dob" size="50" name="dob" required> <br><br>
        <label for="crnt-loc"> CURRENT LOCATION : </label>
        <input type="text" id="crnt-loc" size="50" name="current_location" required><br> <br>
        <label for="e-contact"> EMERGENCY CONTACT NUMBER : </label>
        <input type="text" id="e-contact" size="50" name="e_contact_number" required> <br><br>
        <label for="e-name"> EMERGENCY CONTACT NAME : </label>
        <input type="text" id="e-name" size="50" name="e_contact_name" required>
        <div class="mandatory-form">
        <p class= "mandatory-title"> QUESTIONNAIRE (ANSWERS TO ALL QUESTIONS IS MANDATORY) </p>
        <p> 1. Have you come in contact with any person who has come from foreign country within last 
            one month? (Yes/ No). If Yes provide details (When / Where / Relationship with the person. </p>
            Yes <input type="radio" id="quesYes" value="Yes" onclick="EnableDisableTextBox(this)" />
            No <input type="radio" value="No" id="quesNo" onclick="EnableDisableTextBox(this)" />
            <input type="text" id="mandatoryQuestions" disabled = "disabled" /> <br>
            <p> 2. Have you been in contact or in close physical proximity with a COVID-19 positive 
                patient, or someone with symptoms of Covid-19 i.e. dry cough, cold with fever (Yes/ No). 
                If Yes provide details. </p>
            Yes <input type="radio" id="quesYes" value="Yes" onclick="EnableDisableTextBox(this)" />
            No <input type="radio" value="No" id="quesNo" onclick="EnableDisableTextBox(this)" />
            <input type="text" id="mandatoryQuestions" disabled = "disabled" /> <br>
            <p>3. Have you been unwell and visited any doctor or Health facility (Path lab or Hospital)
                 for treatment recently? (Yes/ No). If Yes, provide details of your sickness: </p>
            Yes <input type="radio" id="quesYes" value="Yes" onclick="EnableDisableTextBox(this)" />
            No <input type="radio" value="No" id="quesNo" onclick="EnableDisableTextBox(this)" />
            <input type="text" id="mandatoryQuestions" disabled = "disabled" /> <br>
            <p> 4. Have you travelled within the Country in the past 1 month (Yes/No). If Yes 
                provide travel details.  </p>
            Yes <input type="radio" id="quesYes" value="Yes" onclick="EnableDisableTextBox(this)" />
            No <input type="radio" value="No" id="quesNo" onclick="EnableDisableTextBox(this)" />
            <input type="text" id="mandatoryQuestions" disabled = "disabled" /> <br>   
            <p> 5. Do you or any of your immediate family members had any of the following symptoms?  </p>
            <div class="quesFive">
            <label> a. Fever :
            <input type="text" /> <br> </label> <br>
            <label> b. Cough/ Sore Throat :
            <input type="text" /> <br> </label> <br>
            <label> c. Breathlessness :
            <input type="text" /> <br> </label> <br>
            </div>
            <p> 6. Are you under any medication? (Yes/ No). If Yes provide Details: </p>
            Yes <input type="radio" id="quesYes" value="Yes" onclick="EnableDisableTextBox(this)" />
            No <input type="radio" value="No" id="quesNo" onclick="EnableDisableTextBox(this)" />
            <input type="text" id="mandatoryQuestions" disabled = "disabled" /> <br>
                <p class="declaration"> DECLARATION </p>
                <span> I Mr./ Ms. </span> <input type="text" > <span> Emp.Code: </span> <input type="text" >
                <span> hereby declare that, I am aware of the Coronavirus outbreak as a global health emergency and
                     will be joining office after the mandatory lockdown period in the city is over. After joining 
                     office, and in order to protect yourself and others around you from the Covid 19 virus (
                         as per WHO guidelines), we request  you to   take due precautions of for yourself,
                          your team-members, and colleagues, both while in office and while travelling to and 
                          fro home and office. . We advise you to proactively seek medical advice/ treatment for 
                          any symptoms similar to the COVID-19 and keep the organization informed. All details 
                          provided above are true to the best of my knowledge.</span> <br>
            <div class="declaration-sign-name"> 
                <label for="signature"> Signature :</label> <input type="text"  > <br>
            <label for="dec-name"> Name : </label> <input type="text" required> <br>
                     </div>
            <!-- Date : </label> <input type="text" required > -->
        </div>
        <input type="submit" value="submit">
    </form>
</div>
</body>
</html>